﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLib
{
    class MissileData : Component
    {
        public float Velocity;
        public float Radius;
        public float MaxDistance;
        public float IsAOE;
        public float AOERadius;
    }
}
